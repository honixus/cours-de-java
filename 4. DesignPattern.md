#  Cours de Java - Design Pattern

<center>
    <img src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png" width="200"></td>
</center>

<h2>Pluquet Frédéric, HELHa, Année académique 2023-2024</h2>

<h2>Sommaire</h2>

* 1. [Dessign Patterns, c'est quoi ?](#DesignPatternCestQuoi)
* 2. [Catégories de Design patterns](#DesignPatternsCategories)
* 3. [Creational design patterns(Creational Patterns)](#CreationalDesignPatterns)
	* 3.1. [Singleton](#CP_Singleton)
* 4. [Structural design patterns(Structural Patterns)](#StructuralDesignPatterns)
	* 4.1. [Composite](#SP_Composite)
* 5. [Behavioral design patterns(Behavioral Patterns)](#BehavioralDesignPatterns)
	* 5.1. [Observer](#BP_Observer)

##  1. <a name='#DesignPatternCestQuoi'></a>Dessign Pattern, c'est quoi ?
- En génie logiciel, un design pattern est une solution générale et réutilisable à un problème récurrent dans la conception de logiciels. Un design pattern n'est pas une conception achevée qui peut être 
transformée directement en code. C'est une description ou un modèle de solution pour résoudre un problème, utilisable dans de nombreuses situations différentes.
##  2. <a name='#DesignPatternsCategories'></a>Différentes Catégories de Design Patterns
- Patterns de création (```Creational Patterns```) : Ces patterns concernent le processus de création des objets. Ils permettent de créer des objets de manière contrôlée et abstraite.
- Patterns structurels (```Structural Patterns```) : Ces patterns concernent la composition des classes et des objets. Ils aident à garantir que si une partie du système change, le reste du système ne sera pas trop affecté.
- Patterns comportementaux (```Behavioral Patterns```) : Ces patterns concernent les interactions et les responsabilités entre les objets.
##  3. <a name='#CreationalDesignPatterns'></a>Creational design patterns 
###  3.1. <a name='CP_Singleton'></a>Singleton
####  3.1.1. <a name='Intro_CP_Singleton'></a>Solution au problème récurrent ?
- ```Le Singleton``` est utilisé pour résoudre le problème quand on veut une instance unique d'une classe dans un programme
- Pour cela le ```Le Singleton``` s'assure qu'on ait UNE instant de la sorte qui existe en fournissant que un seul point d'accès à cette instance
####  3.1.2. <a name='CP_Singleton_Vulgarisation'></a>Vulgarisation du Singleton?
- Prenons un exemple Simple : Dans une école, chaque classe ne peut avoir qu'un seul professeur à la fois. De la même manière, le pattern Singleton assure qu'il n'y a qu'une seule instance d'une classe dans toute l'application.
- Ici dans ```Le Singleton``` c'est la même idée qu'on retrouve.
####  3.1.3. <a name='CP_Singleton_ExempleCode'></a>Exemple avec Code
- Exemple du ```Singleton``` avec la même idée:
- Classe ```Professeur```
```java 
public class Professeur {
    // Instance statique unique de la classe Professeur
    private static Professeur instance;
    
    // Nom du professeur
    private String nom;

    // Constructeur privé pour empêcher la création de nouvelles instances
    private Professeur(String nom) {
        this.nom = nom;
    }

    // Méthode publique statique pour obtenir l'instance unique de la classe
    public static Professeur getInstance(String nom) {
        if (instance == null) {
            instance = new Professeur(nom);
        }
        return instance;
    }

    // Méthode pour obtenir le nom du professeur
    public String getNom() {
        return nom;
    }
}
```
    
- Classe ```Ecole``` 
```java
public class Ecole {
    public static void main(String[] args) {
        // Obtient l'instance unique du professeur pour la classe
        Professeur professeur1 = Professeur.getInstance("M. Pluquet");

        // Affiche le nom du professeur
        System.out.println("Le professeur de la classe est : " + professeur1.getNom());

        // Essaye de créer une nouvelle instance de Professeur pour la même classe
        Professeur professeur2 = Professeur.getInstance("Mme Casier");

        // Affiche le nom du professeur pour vérifier qu'il n'y a qu'une seule instance
        System.out.println("Le professeur de la classe est maintenant : " + professeur2.getNom());
    }
}
```
- Console lors de l'execution du main

	Le professeur de la classe est : M. Pluquet

	Le professeur de la classe est maintenant : M. Pluquet

	Process finished with exit code 0

- On peut voir ici qui l'instance de ```professeur2``` avec Mme Casier est ignoré tant que l'instance ```professeur1``` occupe l'instance 

- RAPPEL : on peut utiliser le Synchronized vue dans le module 1. Java.md 
- Classe ```Professeur``` AVEC Synchronized sur la Méthode 
```java 
public class Professeur {
    // Instance statique unique de la classe Professeur
    private static Professeur instance;

    // Nom du professeur
    private String nom;

    // Constructeur privé pour empêcher la création de nouvelles instances
    private Professeur(String nom) {
        this.nom = nom;
    }

    // Méthode publique statique synchronisée pour obtenir l'instance unique de la classe
    public static synchronized Professeur getInstance(String nom) {
        if (instance == null) {
            instance = new Professeur(nom);
        }
        return instance;
    }

    // Méthode pour obtenir le nom du professeur
    public String getNom() {
        return nom;
    }
}
```
- SUITE RAPPEL : on peut utiliser le Synchronized + Object du nom de lock vue dans le module 1. Java.md également
- Classe ```Professeur``` AVEC Synchronized + Object lock dans la méthode 
```java 
public class Professeur {
    // Instance statique unique de la classe Professeur
    private static volatile Professeur instance;

    // Nom du professeur
    private String nom;

    // Objet verrou pour synchroniser le bloc critique
    private static final Object lock = new Object();

    // Constructeur privé pour empêcher la création de nouvelles instances
    private Professeur(String nom) {
        this.nom = nom;
    }

    // Méthode publique statique pour obtenir l'instance unique de la classe
    public static Professeur getInstance(String nom) {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new Professeur(nom);
                }
            }
        }
        return instance;
    }

    // Méthode pour obtenir le nom du professeur
    public String getNom() {
        return nom;
    }
}
```
##  4. <a name='#StructuralDesignPatterns'></a>Structural Design Patterns
###  4.1. <a name='SP_Composite'></a>Composite


##  5. <a name='#BehavioralDesignPatterns'></a>Behavioral Design Patterns
###  5.1. <a name='BP_Observer'></a>Observer
